﻿using System.Collections.Generic;
using System.Linq;
using GeneratorLib.Executers.General;

namespace TestLib
{
  public static class Helpers
  {
    public static int[] GetBinarySequence(double[] sequence)
    {
      return sequence.Select(element => (int)element % 2).ToArray();
    }
  }
}
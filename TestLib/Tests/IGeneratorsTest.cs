﻿using GeneratorLib.Executers.Binary;
using GeneratorLib.Executers.General;

namespace TestLib.Tests
{
  public interface IGeneratorsTest
  {
    double TestResult(params int[] sequence);
    double TestResult(IBinaryGeneratorExecuter binaryGeneratorExecuter, int sequenceSize);
    double TestResult(IGeneratorExecuter binaryGeneratorExecuter, int sequenceSize);
  }
}
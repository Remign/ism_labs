﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneratorLib.Executers.Binary;
using GeneratorLib.Executers.General;
using MicrosoftResearch.Infer.Collections;
using MicrosoftResearch.Infer.Maths;

namespace TestLib.Tests
{
  public class RandomExcursionsVariantTest : IGeneratorsTest
  {
    public double TestResult(params int[] sequence)
    {
      return RunTest(sequence);
    }

    public double TestResult(IBinaryGeneratorExecuter binaryGeneratorExecuter, int sequenceSize)
    {
      var sequence = binaryGeneratorExecuter.GetBinarySequence(sequenceSize);

      return RunTest(sequence);
    }

    public double TestResult(IGeneratorExecuter binaryGeneratorExecuter, int sequenceSize)
    {
      var sequence = Helpers.GetBinarySequence(binaryGeneratorExecuter.GetSequence(sequenceSize));

      return RunTest(sequence);
    }

    private double RunTest(int[] sequence)
    {
      var xArray = sequence.Select(variable => variable * 2 - 1).ToArray();

      var sList = xArray.Select((t, i) => xArray.Take(i + 1).Sum()).ToList();

      //for (var i = 0; i < xArray.Length; ++i)
      //{
      //  var sum = 0;
      //  for (var j = 0; j < i; j++)
      //  {
      //    sum += xArray[j];
      //  }
      //  sList.Add(sum);
      //}

      var eDictionary = new Dictionary<int, int>();

      var jCount = sList.Count(value => value == 0) + 1;

      var keys = sList.GroupBy(i => i).Where(i => i.Key != 0).Select(i => i.Key).ToArray();
      var values = sList.GroupBy(i => i).Where(i => i.Key != 0).Select(i => i.Count()).ToArray();
      for (var i = 0; i < keys.Length; ++i)
      {
        eDictionary.Add(keys[i], values[i]);
      }

      Func<int, double> selector = key => Math.Abs(eDictionary[key] - jCount) / Math.Sqrt(2 * jCount * (4 * Math.Abs(key) - 2));

      var pValues = eDictionary.Keys.
        Select(selector).
        Select(MMath.Erfc).
        ToList();

      return pValues.Min();
    }
  }
}
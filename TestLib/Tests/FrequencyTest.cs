﻿using System;
using System.Linq;
using GeneratorLib.Executers.Binary;
using GeneratorLib.Executers.General;
using MicrosoftResearch.Infer.Maths;

namespace TestLib.Tests
{
  public class FrequencyTest : IGeneratorsTest
  {
    public double TestResult(params int[] sequence)
    {
      return RunTest(sequence);
    }

    public double TestResult(IBinaryGeneratorExecuter binaryGeneratorExecuter, int sequenceSize)
    {
      var sequence = binaryGeneratorExecuter.GetBinarySequence(sequenceSize);

      return RunTest(sequence);
    }

    public double TestResult(IGeneratorExecuter binaryGeneratorExecuter, int sequenceSize)
    {
      var sequence = Helpers.GetBinarySequence(binaryGeneratorExecuter.GetSequence(sequenceSize));

      return RunTest(sequence);
    }

    private double RunTest(int[] sequence)
    {
      var absValue = sequence.Sum(variable => variable * 2 - 1);

      var testStatistics = Math.Abs(absValue) / Math.Sqrt(sequence.Length);

      var pValue = MMath.Erfc(testStatistics / Math.Sqrt(2));

      return pValue;
    }
  }
}

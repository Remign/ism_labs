﻿using System;
using GeneratorLib.Executers.Binary;
using GeneratorLib.Executers.General;

namespace TestLib.Tests
{
  public class BinaryMatrixRankTest : IGeneratorsTest
  {
    public double TestResult(params int[] sequence)
    {
      return RunTest(sequence);
    }

    public double TestResult(IBinaryGeneratorExecuter binaryGeneratorExecuter, int sequenceSize)
    {
      var sequence = binaryGeneratorExecuter.GetBinarySequence(sequenceSize);

      return RunTest(sequence);
    }

    public double TestResult(IGeneratorExecuter binaryGeneratorExecuter, int sequenceSize)
    {
      var sequence = Helpers.GetBinarySequence(binaryGeneratorExecuter.GetSequence(sequenceSize));

      return RunTest(sequence);
    }

    private double RunTest(int[] sequence)
    {
      int M = 32, Q = 32;
      //	        int M = 3, Q = 3;
      int n = sequence.Length;
      //	        int n = 20;
      int N = n / (M * Q);
      //	        int N = 2;
      double[] p = { 0.2888, 0.5776, 0.1336 };
      double[] f = { 0, 0, 0 };

      for (int i = 0; i < N; i++)
      {
        int[][] matrix = new int[M][];
        for (int j = 0; j < M; j++)
        {
          matrix[j] = new int[Q];
          for (int k = 0; k < Q; k++)
          {
            var value = i * M * Q + j * M + k;
            matrix[j][k] = sequence[value];
          }
        }
        int rank = computeRank(M, Q, matrix);
        if (rank == M)
        {
          f[0]++;
        }
        else if (rank == M - 1)
        {
          f[1]++;
        }
        else
        {
          f[2]++;
        }
      }

      return Math.Exp((-1) * chiSquareValue(f, p, N) / 2);
    }

    private double chiSquareValue(double[] a, double[] b, int n)
    {
      return chiSquareValue(a, b, n, b.Length - 1);
    }

    public double chiSquareValue(double[] a, double[] b, int n, int k)
    {
      double res = 0;
      for (int i = 0; i <= k; i++)
      {
        res += Math.Pow(a[i] - n * b[i], 2) / (n * b[i]);
      }
      return res;
    }



    private int computeRank(int M, int Q, int[][] matrix)
    {
      int i, rank, m = Math.Min(M, Q);

      /* FORWARD APPLICATION OF ELEMENTARY ROW OPERATIONS */
      for (i = 0; i < m - 1; i++)
      {
        if (matrix[i][i] == 1)
        {
          perform_elementary_row_operations(0, i, M, Q, matrix);
        }
        else
        { 	/* matrix[i][i] = 0 */

          if (find_unit_element_and_swap(0, i, M, Q, matrix) == 1)
          {
            perform_elementary_row_operations(0, i, M, Q, matrix);
          }
        }
      }

      /* BACKWARD APPLICATION OF ELEMENTARY ROW OPERATIONS */
      for (i = m - 1; i > 0; i--)
      {
        if (matrix[i][i] == 1)
        {
          perform_elementary_row_operations(1, i, M, Q, matrix);
        }
        else
        { 	/* matrix[i][i] = 0 */

          if (find_unit_element_and_swap(1, i, M, Q, matrix) == 1)
          {
            perform_elementary_row_operations(1, i, M, Q, matrix);
          }
        }
      }

      rank = determine_rank(m, M, Q, matrix);

      return rank;
    }

    private void perform_elementary_row_operations(int flag, int i, int M, int Q, int[][] A)
    {
      int j, k;

      if (flag == 0)
      {
        for (j = i + 1; j < M; j++)
        {
          if (A[j][i] == 1)
          {
            for (k = i; k < Q; k++)
            {
              A[j][k] = (A[j][k] + A[i][k]) % 2;
            }
          }
        }
      }
      else
      {
        for (j = i - 1; j >= 0; j--)
        {
          if (A[j][i] == 1)
          {
            for (k = 0; k < Q; k++)
            {
              A[j][k] = (A[j][k] + A[i][k]) % 2;
            }
          }
        }
      }
    }

    private int find_unit_element_and_swap(int flag, int i, int M, int Q, int[][] A)
    {
      int index, row_op = 0;

      if (flag == 0)
      {
        index = i + 1;
        while ((index < M) && (A[index][i] == 0))
        {
          index++;
        }
        if (index < M)
        {
          row_op = swap_rows(i, index, Q, A);
        }
      }
      else
      {
        index = i - 1;
        while ((index >= 0) && (A[index][i] == 0))
        {
          index--;
        }
        if (index >= 0)
        {
          row_op = swap_rows(i, index, Q, A);
        }
      }

      return row_op;
    }

    private int swap_rows(int i, int index, int Q, int[][] A)
    {
      int p;
      int temp;

      for (p = 0; p < Q; p++)
      {
        temp = A[i][p];
        A[i][p] = A[index][p];
        A[index][p] = temp;
      }

      return 1;
    }

    private int determine_rank(int m, int M, int Q, int[][] A)
    {
      int i, j, rank, allZeroes;

      /* DETERMINE RANK, THAT IS, COUNT THE NUMBER OF NONZERO ROWS */
      rank = m;
      for (i = 0; i < M; i++)
      {
        allZeroes = 1;
        for (j = 0; j < Q; j++)
        {
          if (A[i][j] == 1)
          {
            allZeroes = 0;
            break;
          }
        }
        if (allZeroes == 1)
        {
          rank--;
        }
      }

      return rank;
    }
  }
}
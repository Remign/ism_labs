﻿using System;
using System.Drawing;
using System.Windows.Forms;
using GeneratorLib.Executers;
using GeneratorLib.Executers.General;
using ZedGraph;

namespace GeneratorVisualization
{
  public partial class Form1 : Form
  {
    private LineItem _myCurve;
    private readonly IGeneratorExecuter _weakGeneratorExecuter;
    private readonly IGeneratorExecuter _strongGeneratorExecuter;
    private readonly IGeneratorExecuter _maclarenGeneratorStrongExecuter;
    private readonly IGeneratorExecuter _maclarenGeneratorWeakExecuter;

    public Form1()
    {
      _weakGeneratorExecuter = Executers.WeakGenerator;
      _strongGeneratorExecuter = Executers.StrongGenerator;

      _maclarenGeneratorWeakExecuter = Executers.MaclarenGeneratorWeak;
      _maclarenGeneratorStrongExecuter = Executers.MaclarenGeneratorStrong;
      
      InitializeComponent();
    }

    private void Form1_Resize(object sender, EventArgs e)
    {
      SetSize();
    }

    // SetSize() is separate from Resize() so we can 
    // call it independently from the Form1_Load() method
    // This leaves a 10 px margin around the outside of the control
    // Customize this to fit your needs
    private void SetSize()
    {
      zedGraphControl1.Location = new Point(10, 10);
      // Leave a small margin around the outside of the control
      zedGraphControl1.Size = new Size(ClientRectangle.Width - 20,
                              ClientRectangle.Height - 20);
    }

    // Respond to form 'Load' event
    private void Form1_Load(object sender, EventArgs e)
    {
      // Setup the graph
      CreateGraph(zedGraphControl1);
      // Size the control to fill the form with a margin
      SetSize();
    }

    // Build the Chart
    private void CreateGraph(ZedGraphControl zgc)
    {
      var list1 = new PointPairList();
      
      var myPane = zgc.GraphPane;

      _myCurve = myPane.AddCurve("Sequence",
            list1, Color.Red, SymbolType.Circle);

      _myCurve.Line.IsVisible = false;
      _myCurve.Symbol.Fill = new Fill(Color.Red);
      _myCurve.Symbol.Size = (float)3.0;

      zgc.AxisChange();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      zedGraphControl1.GraphPane.Title.Text = "1st seq";

      this.SetCurve(_weakGeneratorExecuter);
    }

    private void button2_Click(object sender, EventArgs e)
    {
      zedGraphControl1.GraphPane.Title.Text = "2nd seq";

      this.SetCurve(_strongGeneratorExecuter);
    }

    private void button3_Click(object sender, EventArgs e)
    {
      zedGraphControl1.GraphPane.Title.Text = "3rd seq";

      this.SetCurve(_maclarenGeneratorWeakExecuter);
    }

    private void button4_Click(object sender, EventArgs e)
    {
      zedGraphControl1.GraphPane.Title.Text = "4th seq";

      this.SetCurve(_maclarenGeneratorStrongExecuter);
    }

    private void SetCurve(IGeneratorExecuter executer)
    {
      _myCurve.Clear();
      var sequence = executer.GetZeroToOneSequence(1730);

      for (var i = 0; i < sequence.Length - 1; ++i)
      {
        _myCurve.AddPoint(sequence[i], sequence[i + 1]);
      }

      zedGraphControl1.Refresh();
    }
  }
}

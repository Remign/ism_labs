﻿namespace GeneratorLib.Generators.Binary
{
  public class LfsrGenerator : IBinaryGenerator
  {
    private long _register = 1L;
    private readonly int[] _coefficients;

    public LfsrGenerator(params int[] coefficients)
    {
      _coefficients = coefficients;
    }

    public int NextBit()
    {
      long temp = 0;
      foreach (var coefficient in _coefficients)
      {
        temp ^= (_register >> (coefficient - 1));
      }
      temp &= 1L;
      _register = (temp << (_coefficients[0] - 1)) | (_register >> 1);

      return (int)(_register & 1L);
    }
  }
}
﻿namespace GeneratorLib.Generators.Binary
{
  public interface IBinaryGenerator
  {
    int NextBit();
  }
}
﻿namespace GeneratorLib.Generators.Binary
{
  public class SelfShrinkingGenerator : IBinaryGenerator
  {
    private readonly IBinaryGenerator _binaryGenerator;

    public SelfShrinkingGenerator(IBinaryGenerator lfsrGenerator)
    {
      _binaryGenerator = lfsrGenerator;
    }

    public int NextBit()
    {
      var first = _binaryGenerator.NextBit();
      var second = _binaryGenerator.NextBit();

      if (first == 1 && second == 0)
      {
        return 0;
      }

      if (first == 1 && second == 1)
      {
        return 1;
      }

      return this.NextBit();
    }
  }
}
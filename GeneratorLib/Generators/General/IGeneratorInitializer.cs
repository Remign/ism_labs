﻿namespace GeneratorLib.Generators.General
{
  public interface IGeneratorInitializer
  {
    IGenerator InitializeSeed(int value = 0);

    long Module { get; }

    double ExpectedValue { get; }

    double Dispersion { get; }
  }
}
﻿using System;
using System.Linq;
using GeneratorLib.Executers.General;

namespace GeneratorLib.Generators.General
{
  public class HiSquareDistribution : IGenerator, IGeneratorInitializer
  {
    private readonly int _m;
    private readonly NormalDistribution _normalDistribution;

    private double[] _normalDistributionSequence;
    private int _index;

    public HiSquareDistribution(int m, NormalDistribution normalDistributionSequence)
    {
      _m = m;
      _normalDistribution = normalDistributionSequence;
      ExpectedValue = _m;
      Dispersion = 2 * _m;
    }


    public void Dispose()
    {
    }

    public double GetRandomValue()
    {
      var basicSequence = _normalDistributionSequence.Skip(_index++ * _m).Take(_m);

      return basicSequence.Sum(basicValue => basicValue * basicValue);
    }

    public IGenerator InitializeSeed(int value = 0)
    {
      _normalDistributionSequence = new GeneratorExecuter(_normalDistribution).GetSequence(value * _m);
      _index = 0;

      return this;
    }

    public long Module { get; private set; }

    public double ExpectedValue { get; private set; }
    public double Dispersion { get; private set; }
  }
}
﻿using System;

namespace GeneratorLib.Generators.General
{
  public class CongruentGenerator : IGenerator, IGeneratorInitializer
  {
    private readonly long _a = 253801L;
    private readonly long _c = 14519L;
    private readonly long _m = 4294967295L;
    private long _next;

    public CongruentGenerator()
    {
      var unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
      _next = unixTimestamp;
    }

    public CongruentGenerator(int a, int c, int m) : this()
    {
      _a = a;
      _c = c;
      _m = m;
    }

    public CongruentGenerator(long seed, int a, int c, int m)
      : this(a, c, m)
    {
      _next = seed;
    }

    double IGenerator.GetRandomValue()
    {
      //var prev = _next;
      _next = (_a * _next + _c) % _m;
      //return prev;
      return _next;
    }

    long IGeneratorInitializer.Module
    {
      get { return _m; }
    }

    public double ExpectedValue { get; private set; }
    public double Dispersion { get; private set; }

    IGenerator IGeneratorInitializer.InitializeSeed(int value)
    {
      var unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
      _next = unixTimestamp;
      return this;
    }

    void IDisposable.Dispose()
    {
    }
  }
}

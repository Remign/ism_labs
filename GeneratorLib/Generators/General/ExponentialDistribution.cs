﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneratorLib.Executers.Binary;

namespace GeneratorLib.Generators.General
{
  public class ExponentialDistribution : IGenerator, IGeneratorInitializer
  {
    private readonly int _lambda;
    private readonly IBinaryGeneratorExecuter _binaryExecuter;

    private int _sequenceSize;
    private double[] _randomSequence;
    private int _index;


    public ExponentialDistribution(int lambda, IBinaryGeneratorExecuter binaryExecuter)
    {
      _lambda = lambda;
      _binaryExecuter = binaryExecuter;
    }

    public void Dispose()
    {
    }

    public double GetRandomValue()
    {
      return (-1) * Math.Log(_randomSequence[_index++]);
    }

    public IGenerator InitializeSeed(int value = 0)
    {
      _sequenceSize = value;
      _index = 0;
      _randomSequence = _binaryExecuter.ZeroToOneSequence(_sequenceSize);

      return this;
    }

    public long Module { get; private set; }
    public double ExpectedValue { get; private set; }
    public double Dispersion { get; private set; }
  }
}

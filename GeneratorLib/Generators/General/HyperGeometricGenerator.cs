﻿using System;
using System.Security.Cryptography.X509Certificates;
using GeneratorLib.Executers.Binary;

namespace GeneratorLib.Generators.General
{
  public class HyperGeometricGenerator : IGenerator, IGeneratorInitializer
  {
    private readonly int _checkedCount = 20;
    private readonly int _successfulCount = 20;
    private readonly int _totalCount = 30;
    private readonly IBinaryGeneratorExecuter _binaryExecuter;

    public HyperGeometricGenerator()
    {
    }

    public HyperGeometricGenerator(int checkedCount, int successfulCount, int totalCount, IBinaryGeneratorExecuter binaryExecuter)
    {
      _checkedCount = checkedCount;
      _successfulCount = successfulCount;
      _totalCount = totalCount;
      _binaryExecuter = binaryExecuter;
      ExpectedValue = (double)_checkedCount * _successfulCount / _totalCount;
      var a = (ExpectedValue * (1 - (double)_successfulCount / _totalCount) *
               (_totalCount - _checkedCount));
      var b = (_totalCount - 1);
      Dispersion = a / b;
    }

    public void Dispose()
    {
    }

    public double GetRandomValue()
    {
      long successfulSample = 0;
      var currentSuccessful = _successfulCount;
      var currentTotal = _totalCount;
      var randomSequence = _binaryExecuter.ZeroToOneSequence(_checkedCount);

      for (var i = 0; i < _checkedCount; ++i)
      {
        var value = (double)currentSuccessful / currentTotal;

        if (randomSequence[i] <= value)
        {
          successfulSample++;
          currentSuccessful--;
          currentTotal--;
        }
        else
        {
          currentTotal--;
        }
      }

      return successfulSample;
    }

    public IGenerator InitializeSeed(int value = 0)
    {
      return this;
    }

    public long Module { get; private set; }
    public double ExpectedValue { get; private set; }
    public double Dispersion { get; private set; }
  }
}
﻿using System;

namespace GeneratorLib.Generators.General
{
  public interface IGenerator : IDisposable
  {
    double GetRandomValue();
  }
}

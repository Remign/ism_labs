﻿using System;
using GeneratorLib.Executers.General;

namespace GeneratorLib.Generators.General
{
  public class MacLarenMarsagliaGenerator : IGenerator, IGeneratorInitializer
  {
    private readonly IGeneratorExecuter _firstGenerator;
    private readonly IGeneratorExecuter _secondGenerator;

    private readonly int _k;

    private double[] _firstSequence;
    private double[] _secondSequence;

    private double[] _referenceTable;

    private double _next;
    private int _index;

    private int _size;

    public MacLarenMarsagliaGenerator(IGeneratorExecuter firstGenerator, IGeneratorExecuter secondGenerator, int k = 64)//int k = 32064128)
    {
      _firstGenerator = firstGenerator;
      _secondGenerator = secondGenerator;
      _k = k;
    }
    double IGenerator.GetRandomValue()
    {
      // ReSharper disable once PossibleLossOfFraction
      var value = _k * _secondSequence[_index] / _secondGenerator.GeneratorModule;
      var calculatedIndex = (int)Math.Round(value, MidpointRounding.AwayFromZero);
      _next = _referenceTable[calculatedIndex];
      _referenceTable[calculatedIndex] = _firstSequence[_index + _k];
      _index++;
      return _next;
    }

    long IGeneratorInitializer.Module
    {
      get { return _firstGenerator.GeneratorModule; }
    }

    public double ExpectedValue { get; private set; }
    public double Dispersion { get; private set; }

    IGenerator IGeneratorInitializer.InitializeSeed(int value)
    {
      _size = value;
      _firstSequence = _firstGenerator.GetSequence(_size + _k);
      _secondSequence = _secondGenerator.GetSequence(_size);
      _referenceTable = new double[_k];
      for (var i = 0; i < _k; ++i)
      {
        _referenceTable[i] = _firstSequence[i];
      }
      _index = 0;
      return this;
    }

    void IDisposable.Dispose()
    {
      _firstSequence = null;
      _secondSequence = null;
      _referenceTable = null;
    }
  }
}
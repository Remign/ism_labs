﻿using System.Linq;
using GeneratorLib.Executers.Binary;

namespace GeneratorLib.Generators.General
{
  public class NormalDistribution : IGenerator, IGeneratorInitializer
  {
    private double[] _randomSequence;
    private int _index;
    private readonly IBinaryGeneratorExecuter _binaryExecuter;

    public NormalDistribution(IBinaryGeneratorExecuter binaryExecuter)
    {
      _binaryExecuter = binaryExecuter;
      ExpectedValue = 0;
      Dispersion = 1;
    }

    public void Dispose()
    {
    }

    public double GetRandomValue()
    {
      var basicSequence = _randomSequence.Skip(_index++ * 12).Take(12);

      var value = basicSequence.Sum();

      value -= 6;

      return value;
    }

    public IGenerator InitializeSeed(int value = 0)
    {
      _randomSequence = _binaryExecuter.ZeroToOneSequence(value * 12);
      _index = 0;

      return this;
    }

    public long Module { get; private set; }
    public double ExpectedValue { get; private set; }
    public double Dispersion { get; private set; }
  }
}
﻿using GeneratorLib.Executers.Binary;

namespace GeneratorLib.Generators.General
{
  public class UniformDistribution : IGenerator, IGeneratorInitializer
  {
    private readonly int _a;
    private readonly int _b;
    private readonly IBinaryGeneratorExecuter _binaryExecuter;
    private int _sequenceSize;
    private double[] _randomSequence;
    private int _index;

    public UniformDistribution(int a, int b, IBinaryGeneratorExecuter binaryExecuter)
    {
      this._a = a;
      this._b = b;
      _binaryExecuter = binaryExecuter;
    }

    public void Dispose()
    {
    }

    public double GetRandomValue()
    {
      var value = (_b - _a) * _randomSequence[_index++] + _a;

      return value;
    }

    public IGenerator InitializeSeed(int value = 0)
    {
      _sequenceSize = value;
      _index = 0;
      _randomSequence = _binaryExecuter.ZeroToOneSequence(_sequenceSize);
      return this;
    }

    public long Module { get; private set; }
    public double ExpectedValue { get; private set; }
    public double Dispersion { get; private set; }
  }
}
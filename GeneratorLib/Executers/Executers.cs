﻿using GeneratorLib.Executers.Binary;
using GeneratorLib.Executers.General;
using GeneratorLib.Generators.Binary;
using GeneratorLib.Generators.General;

namespace GeneratorLib.Executers
{
  public static class Executers
  {
    public static IGeneratorExecuter WeakGenerator
    {
      get
      {
        return new GeneratorExecuter(new CongruentGenerator(3, 1, 101));
      }
    }

    public static IGeneratorExecuter StrongGenerator
    {
      get
      {
        return new GeneratorExecuter(new CongruentGenerator(17, 1, 2097152));
      }
    }

    public static IGeneratorExecuter MaclarenGeneratorWeak
    {
      get
      {
        return new GeneratorExecuter(new MacLarenMarsagliaGenerator(Executers.WeakGenerator, Executers.StrongGenerator));
      }
    }

    public static IGeneratorExecuter MaclarenGeneratorStrong
    {
      get
      {
        return new GeneratorExecuter(new MacLarenMarsagliaGenerator(Executers.StrongGenerator, Executers.WeakGenerator));
      }
    }

    public static IBinaryGeneratorExecuter SelfShrinkingGenerator
    {
      get
      {
        // return new BinaryGeneratorExecuter(new SelfShrinkingGenerator(new LfsrGenerator(32, 31, 30, 28, 26, 1)));
        return new BinaryGeneratorExecuter(new SelfShrinkingGenerator(new LfsrGenerator(32, 22, 2, 1)));
      }
    }

    public static IGeneratorExecuter HyperGeomentricGenerator
    {
      get
      {
        return new GeneratorExecuter(new HyperGeometricGenerator(20, 20, 30, Executers.SelfShrinkingGenerator));
      }
    }

    public static IGeneratorExecuter UniformDistributor
    {
      get
      {
        return new GeneratorExecuter(new UniformDistribution(20, 30, Executers.SelfShrinkingGenerator));
      }
    }

    public static IGeneratorExecuter NormalDistributor
    {
      get
      {
        return new GeneratorExecuter(new NormalDistribution(Executers.SelfShrinkingGenerator));
      }
    }

    public static IGeneratorExecuter HiSquareDistributor
    {
      get
      {
        return new GeneratorExecuter(new HiSquareDistribution(10, new NormalDistribution(Executers.SelfShrinkingGenerator)));
      }
    }

    public static IGeneratorExecuter ExponentialDistributor
    {
      get { return new GeneratorExecuter(new ExponentialDistribution(1, Executers.SelfShrinkingGenerator)); }
    }
  }
}
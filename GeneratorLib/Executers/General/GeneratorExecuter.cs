﻿using System.Collections.Generic;
using System.Linq;
using GeneratorLib.Generators;
using GeneratorLib.Generators.General;

namespace GeneratorLib.Executers.General
{
  public class GeneratorExecuter : IGeneratorExecuter
  {
    private readonly IGeneratorInitializer _generatorInitializer;

    public GeneratorExecuter(IGeneratorInitializer generator)
    {
      _generatorInitializer = generator;
    }

    public double[] GetSequence(int sequenceSize, int minValue, int maxValue)
    {
      return this.GetSequence(sequenceSize).Select(value => value % maxValue + minValue).ToArray();
    }

    public double[] GetSequence(int sequenceSize)
    {
      var sequence = new List<double>();
      var generator = _generatorInitializer.InitializeSeed(sequenceSize);
      using (generator)
      {
        for (var i = 0; i < sequenceSize; ++i)
        {
          sequence.Add(generator.GetRandomValue());
        }
      }

      return sequence.ToArray();
    }

    long IGeneratorExecuter.GeneratorModule
    {
      get { return _generatorInitializer.Module; }
    }

    double IGeneratorExecuter.ExpectedValue
    {
      get { return _generatorInitializer.ExpectedValue; }
    }


    double IGeneratorExecuter.Dispersion
    {
      get { return _generatorInitializer.Dispersion; }
    }

    string IGeneratorExecuter.GetName
    {
      get { return _generatorInitializer.GetType().Name; }
    }

    public double[] GetZeroToOneSequence(int sequenceSize)
    {
      return this.GetSequence(sequenceSize).Select(value => (double)value / _generatorInitializer.Module).ToArray();
    }
  }
}

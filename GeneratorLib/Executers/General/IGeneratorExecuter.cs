﻿namespace GeneratorLib.Executers.General
{
  public interface IGeneratorExecuter
  {
    double[] GetSequence(int sequenceSize);

    double[] GetSequence(int sequenceSize, int minValue, int maxValue);

    double[] GetZeroToOneSequence(int sequenceSize);

    long GeneratorModule { get; }

    double Dispersion { get; }

    double ExpectedValue { get; }

    string GetName { get; }
  }
}
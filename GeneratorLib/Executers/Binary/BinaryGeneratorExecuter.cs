﻿using System;
using System.Collections.Generic;
using GeneratorLib.Generators.Binary;

namespace GeneratorLib.Executers.Binary
{
  public class BinaryGeneratorExecuter : IBinaryGeneratorExecuter
  {
    private readonly IBinaryGenerator _binaryGenerator;

    private const int NumOfBits = 8;
    private const int MaxValue = 255;

    public BinaryGeneratorExecuter(IBinaryGenerator binaryGenerator)
    {
      _binaryGenerator = binaryGenerator;
    }

    public int[] GetBinarySequence(int sequenceSize)
    {
      var sequence = new List<int>();

      for (var i = 0; i < sequenceSize; ++i)
      {
        sequence.Add(_binaryGenerator.NextBit());
      }

      return sequence.ToArray();
    }

    public double[] ZeroToOneSequence(int sequenceSize)
    {
      var sequence = new List<double>();

      for (var i = 0; i < sequenceSize; ++i)
      {
        var binarySequence = this.GetBinarySequence(NumOfBits);
        var decValue = 0;
        for (var j = 0; j < NumOfBits; ++j)
        {
          decValue += (int)Math.Pow(2, j) * binarySequence[j];
        }

        var valueToAdd = (double)decValue / MaxValue;

        sequence.Add(valueToAdd);
      }

      return sequence.ToArray();

    }
  }
}
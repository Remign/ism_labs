﻿namespace GeneratorLib.Executers.Binary
{
  public interface IBinaryGeneratorExecuter
  {
    int[] GetBinarySequence(int sequenceSize);

    double[] ZeroToOneSequence(int sequenceSize);
  }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using Accord.Statistics.Testing;
using GeneratorLib.Executers;
using GeneratorLib.Executers.General;
using GeneratorLib.Generators.General;
using MonteKarloLibrary;
using TestLib.Tests;

namespace ConsoleApp
{
  class Program
  {
    private static int[] _binarySequence;
    static void Main(string[] args)
    {
      //var weakGeneratorExecuter = new GeneratorExecuter(new CongruentGenerator(7, 7, 500));
      //var strongGeneratorExecuter = new GeneratorExecuter(new CongruentGenerator(17, 1, 2097152));

      //var maclarenGeneratorStrong = new MacLarenMarsagliaGenerator(strongGeneratorExecuter, weakGeneratorExecuter);
      //var maclarenGeneratorWeak = new MacLarenMarsagliaGenerator(strongGeneratorExecuter, weakGeneratorExecuter);

      //var maclarenGeneratorStrongExecuter = new GeneratorExecuter(maclarenGeneratorStrong);
      //var maclarenGeneratorWeakExecuter = new GeneratorExecuter(maclarenGeneratorWeak);

      //foreach (var value in Executers.SelfShrinkingGenerator.GetBinarySequence(50))
      //{
      //  Console.WriteLine(value);
      //}
      //==================================================================================================
      //var charBinarySequence = File.ReadAllText("e.txt").ToCharArray();
      //_binarySequence = charBinarySequence.Select(element => element - 48).ToArray();

      //const int size = 2500;

      //TestResults(new FrequencyTest(), size);
      //TestResults(new BinaryMatrixRankTest(), size);
      //TestResults(new RandomExcursionsVariantTest(), size);

      //var result = new RandomExcursionsVariantTest().TestResult(_binarySequence);

      //Console.WriteLine(result);
      //===================================================================================================
      //Task 4
      var executers = new List<IGeneratorExecuter>
      {
        Executers.HyperGeomentricGenerator,
        Executers.NormalDistributor,
        Executers.HiSquareDistributor
      };

      foreach (var generatorExecuter in executers)
      {
        RunDistribution(generatorExecuter);
      }




      //===================================================================================================
      //Task 5

      //var firstIntegral = new MonteKarloEvaluator(StaticFunctions.Function1);

      //var inf = firstIntegral.EvaluateOneDimansinalFirstIntegralWithHighInfinity(
      //  StaticFunctions.ExponentialDensityDistributionFunction, Executers.ExponentialDistributor);

      //var norm = firstIntegral.EvaluateOneDimansinalFirstIntegral(0, 4,
      //  StaticFunctions.UniformDensityDistributionFunction, Executers.UniformDistributor);

      //var firstResult = inf - norm;

      //var secondIntegral = new MonteKarloEvaluator(StaticFunctions.Function2);

      //var secondResult = secondIntegral.EvaluateTwoDimansinalSecondIntegral(Math.E, Math.PI, Math.Pow(Math.E, 3),
      //  Math.Pow(Math.PI, 3), StaticFunctions.UniformDensityDistributionFunction, Executers.UniformDistributor);

      //Console.WriteLine("First: " + firstResult);
      //Console.WriteLine("Second: " + secondResult);

    }

    private static void RunDistribution(IGeneratorExecuter executer)
    {
      const int sequenceSize = 1530;

      var sequence = executer.GetSequence(sequenceSize);
      Console.WriteLine(executer.GetName);
      Console.Write("Expected Value: ");
      var expectedValue = sequence.Sum() / sequence.Length;
      Console.Write(expectedValue +" <=> ");
      Console.WriteLine(executer.ExpectedValue);
      Console.WriteLine("Dispersion: ");
      var dispersion = sequence.Select(item => Math.Pow(item - expectedValue, 2) / sequence.Length).Sum();
      Console.Write(dispersion + " <=> ");
      Console.WriteLine(executer.Dispersion);
      

      if (executer.GetName == "NormalDistribution")
      {
        var gen = new Accord.Statistics.Distributions.Univariate.NormalDistribution(0, 1);
        //var seq = gen.Generate(sequenceSize);

        var test = new ChiSquareTest(sequence, gen);
        test.Size = 0.037;
        Console.WriteLine("PVal: " + test.PValue + ",ChiTest: " + test.Significant);

        //var test2 = new ChiSquareTest(sequence, seq, 2);
        //Console.WriteLine("PVal: " + test2.PValue + ",ChiTest: " + test2.Significant);
      }
      else if (executer.GetName == "HiSquareDistribution")
      {
        var gen = new Accord.Statistics.Distributions.Univariate.ChiSquareDistribution(10);
        //var seq = gen.Generate(sequenceSize);

        var test = new ChiSquareTest(sequence, gen);
        test.Size = 0.037;
        Console.WriteLine("PVal: " + test.PValue + ",ChiTest: " + test.Significant);
      }
      Console.WriteLine();
    }

    private static double[] GetTestResults(IGeneratorsTest generatorsTest, int sequenceSize)
    {
      var results = new List<double>
      {
        //generatorsTest.TestResult(_binarySequence),
        generatorsTest.TestResult(Executers.WeakGenerator, sequenceSize),
        generatorsTest.TestResult(Executers.StrongGenerator, sequenceSize),
        generatorsTest.TestResult(Executers.MaclarenGeneratorWeak, sequenceSize),
        generatorsTest.TestResult(Executers.MaclarenGeneratorStrong, sequenceSize),
        generatorsTest.TestResult(Executers.SelfShrinkingGenerator, sequenceSize)
      };

      if (generatorsTest.GetType().Name != "RandomExcursionsVariantTest")
      {
        results.Add(generatorsTest.TestResult(_binarySequence));
      }

      return results.ToArray();
    }

    private static void TestResults(IGeneratorsTest generatorsTest, int sequenceSize, string testName = null)
    {
      const double variable = 0.01;
      if (testName == null)
      {
        testName = generatorsTest.GetType().Name;
      }
      Console.WriteLine(testName);
      foreach (var testResult in GetTestResults(generatorsTest, sequenceSize))
      {
        var status = testResult > variable ? "Passed" : "Not passed";
        Console.WriteLine(testResult + " " + status);
      }
      Console.WriteLine();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GeneratorLib.Executers;
using GeneratorLib.Executers.General;
using MicrosoftResearch.Infer.Collections;
using MicrosoftResearch.Infer.Maths;
using Color = System.Drawing.Color;

namespace DistributionGrafics
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
      this.InitializeChart(HyperHeometricChart, 9, 18, Executers.HyperGeomentricGenerator, null, null);
      this.InitializeChart(NormalChart, -5, 5, Executers.NormalDistributor, this.NormalDistributionFunction, this.NormalDensityFunction);
      this.InitializeChart(HiSquareChart, 0, 8, Executers.HiSquareDistributor, this.HiSquareDistributionFunction, this.HiSquareDensityFunction);
    }

    private void InitializeChart(Chart chart, int a, int b, IGeneratorExecuter executer, Func<double, double> function, Func<double, double> density)
    {
      chart.Margin = new Padding(10, 40, 10, 10);

      var chartArea = new ChartArea("Default");

      chart.ChartAreas.Add(chartArea);

      if (function != null)
      {
        chart.Series.Add(new Series("Series1")
        {
          Color = Color.Blue
        });
        chart.Series["Series1"].ChartArea = "Default";
        chart.Series["Series1"].ChartType = SeriesChartType.Line;


        var axisXData = new List<double>();
        var axisYData = new List<double>();
        for (double i = a; i < b; i = i + 0.01)
        {
          axisXData.Add(i);
          axisYData.Add(function(i));
        }

        chart.Series["Series1"].Points.DataBindXY(axisXData, axisYData);
      }

      //
      if (density != null)
      {
        chart.Series.Add(new Series("Series3")
        {
          Color = Color.Red
        });
        chart.Series["Series3"].ChartArea = "Default";
        chart.Series["Series3"].ChartType = SeriesChartType.Line;


        var axisXData3 = new List<double>();
        var axisYData3 = new List<double>();
        for (double i = a; i < b; i = i + 0.01)
        {
          axisXData3.Add(i);
          axisYData3.Add(density(i));
        }

        chart.Series["Series3"].Points.DataBindXY(axisXData3, axisYData3);
      }
      //


      chart.Series.Add(new Series("Series2")
      {
        Color = Color.Green
      });
      chart.Series["Series2"].ChartArea = "Default";
      chart.Series["Series2"].ChartType = SeriesChartType.Line;

      var axisXData2 = new List<double>();
      var axisYData2 = new List<double>();

      var normalSequence = executer.GetSequence(1530);

      for (double i = a; i < b; i = i + 0.01)
      {
        axisXData2.Add(i);
        axisYData2.Add(OwnDistributionFunction(i, normalSequence));
      }

      chart.Series["Series2"].Points.DataBindXY(axisXData2, axisYData2);

    }

    public double NormalDistributionFunction(double x)
    {
      var result = ((double)1 / 2) * (1 + (1 - MMath.Erfc(x / Math.Sqrt(2))));
      return result;
    }

    public double NormalDensityFunction(double x)
    {
      var result = 1 / Math.Sqrt(2 * Math.PI) * Math.Exp((-1) * Math.Pow(x, 2) / 2);
      return result;
    }

    public double HiSquareDistributionFunction(double x)
    {
      var k = 10D;
      var result = MMath.GammaLower(k / 2, x / 2);

      return result;
    }

    public double HiSquareDensityFunction(double x)
    {
      var k = 10D;
      var result = (Math.Pow(0.5, k / 2) / MMath.Gamma(k / 2)) * (Math.Pow(x, k / 2 - 1) * Math.Exp((-1) * x / 2));

      return result;
    }

    public double OwnDistributionFunction(double x, double[] sequence)
    {
      var y = sequence.Select(item => this.Ksi(x - item)).Sum() / sequence.Length;
      return y;
    }

    public double Ksi(double x)
    {
      return x >= 0 ? 1 : 0;
    }

  }
}

﻿using System;

namespace MonteKarloLibrary
{
  public class Integral
  {
    public int[] LowValues;

    public int[] HighValues;

    public StaticFunctions.Function IntegralFunction;

    public Integral(StaticFunctions.Function function, int[] lowValues, int[] highValues)
    {
      IntegralFunction = function;
      LowValues = lowValues;
      HighValues = highValues;
    }
  }
}
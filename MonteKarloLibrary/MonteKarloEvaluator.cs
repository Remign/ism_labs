﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using GeneratorLib.Executers.General;

namespace MonteKarloLibrary
{
  public class MonteKarloEvaluator
  {
    private readonly StaticFunctions.Function _integral;

    private const int RandomSize = 10000;

    //private readonly IGeneratorExecuter _iGeneratorExecuter;

    //private readonly StaticFunctions.Function _densityFunction;

    public MonteKarloEvaluator(StaticFunctions.Function integral1)//, IGeneratorExecuter iGeneratorExecuter)
    {
      _integral = integral1;
      //_iGeneratorExecuter = iGeneratorExecuter;
    }

    public double EvaluateOneDimansinalFirstIntegral(double lowValue, double highValue, StaticFunctions.Function densityFunction, IGeneratorExecuter iGeneratorExecuter)
    {
      var randomSequence = iGeneratorExecuter.GetSequence(RandomSize);

      var sum = randomSequence.Sum(value => _integral(value) / densityFunction(lowValue, highValue));

      var result = sum / RandomSize;

      return result;
    }

    public double EvaluateOneDimansinalFirstIntegralWithHighInfinity(StaticFunctions.Function densityFunction, IGeneratorExecuter iGeneratorExecuter)
    {
      var randomSequence = iGeneratorExecuter.GetSequence(RandomSize);

      var sum = randomSequence.Sum(value => _integral(value) / densityFunction(value));

      var result = sum / RandomSize;

      return result;
    }

    public double EvaluateTwoDimansinalSecondIntegral(double lowValue1, double highValue1, double lowValue2, double highValue2, StaticFunctions.Function densityFunction, IGeneratorExecuter iGeneratorExecuter)
    {
      var randomSequence = iGeneratorExecuter.GetSequence(RandomSize * 2);

      var sum = 0D;

      for (var i = 0; i < randomSequence.Count() / 2; ++i)
      {
        sum += _integral(randomSequence[i], randomSequence[i + RandomSize]) /
               (densityFunction(lowValue1, highValue1) * densityFunction(lowValue2, highValue2));
      }


      var result = sum / RandomSize;

      return result;
    }
  }
}

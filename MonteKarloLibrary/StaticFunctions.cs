﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteKarloLibrary
{
  public static class StaticFunctions
  {
    public delegate double Function(params double[] args);
    public static double Function1(params double[] args)
    {
      return Math.Exp((-1) * args[0]) / (args[0] * Math.Sqrt(1 + Math.Pow(args[0], 3)));
    }

    public static double UniformDensityDistributionFunction(params double[] args)
    {
      return 1 / (args[1] - args[0]);
    }

    public static double ExponentialDensityDistributionFunction(params double[] args)
    {
      return Math.Exp((-1) * args[0]);
    }

    //public static double Function2(params int[] args)
    //{
    //  return 1 / (args[0] * Math.Sqrt(1 + Math.Pow(args[0], 3)));
    //}

    public static double Function2(params double[] args)
    {
      return Math.Atan(args[0] + args[1]);
    }
  }
}
